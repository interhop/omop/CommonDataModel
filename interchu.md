# How to store patient personal informations  ?

In the observation table with SNOMED codes 

4086933	Patient surname
4086449	Patient forename
4307270	Maiden name
4083586 Patient address
4314148 Patient - email address
4245003	Identification number
--4083587	Date of birth

44791058 Patient previous surname
4335813	City of residence
4083592	Patient telephone number
4083591	Patient postal code
4160017 Street address
4177383	Patient mobile telephone number
4014292    Place of birth

4053609 Marital status 
	4338692	Married
	4027529	Separated


# inserted rows into concept

insert into concept values (-1, 'Not yet mapped', 'Metadata', 'None', 'Undefined', null, 'Not yet mapped', '1970-01-01', '2099-01-01',null,null,null);

# triggers on postgres to manage dates

CREATE OR REPLACE FUNCTION omop.dates()
  RETURNS trigger AS
$BODY$
BEGIN
    IF TG_OP = 'INSERT' THEN
        NEW.insert_datetime := now();
        NEW.update_datetime := NULL;
        NEW.change_datetime := now();
    ELSIF TG_OP = 'UPDATE' THEN
        NEW.insert_datetime := OLD.insert_datetime;
        NEW.update_datetime := now();
        NEW.change_datetime := now();
    END IF;
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER person_dates
  BEFORE INSERT OR UPDATE
  ON omop.person
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER visit_occurrence_dates
  BEFORE INSERT OR UPDATE
  ON omop.visit_occurrence
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER visit_detail_dates
  BEFORE INSERT OR UPDATE
  ON omop.visit_detail
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER condition_occurrence_dates
  BEFORE INSERT OR UPDATE
  ON omop.condition_occurrence
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER procedure_occurrence_dates
  BEFORE INSERT OR UPDATE
  ON omop.procedure_occurrence
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER drug_exposure_dates
  BEFORE INSERT OR UPDATE
  ON omop.drug_exposure
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER measurement_physio_dates
  BEFORE INSERT OR UPDATE
  ON omop.measurement_physio
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER measurement_labs_dates
  BEFORE INSERT OR UPDATE
  ON omop.measurement_labs
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER note_content_dates
  BEFORE INSERT OR UPDATE
  ON omop.note_content
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER note_section_dates
  BEFORE INSERT OR UPDATE
  ON omop.note_section
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER concept_dates
  BEFORE INSERT OR UPDATE
  ON omop.concept
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER concept_relationship_dates
  BEFORE INSERT OR UPDATE
  ON omop.concept_relationship
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER concept_synonym_dates
  BEFORE INSERT OR UPDATE
  ON omop.concept_synonym
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER vocabulary_dates
  BEFORE INSERT OR UPDATE
  ON omop.vocabulary
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER cost_dates
  BEFORE INSERT OR UPDATE
  ON omop.cost
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER concept_fhir_dates
  BEFORE INSERT OR UPDATE
  ON omop.concept_fhir
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER cohort_definition_dates
  BEFORE INSERT OR UPDATE
  ON omop.cohort_definition
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER cohort_dates
  BEFORE INSERT OR UPDATE
  ON omop.cohort
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER provider_dates
  BEFORE INSERT OR UPDATE
  ON omop.provider
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER observation_dates
  BEFORE INSERT OR UPDATE
  ON omop.observation
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER care_site_dates
  BEFORE INSERT OR UPDATE
  ON omop.care_site
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER care_site_history_dates
  BEFORE INSERT OR UPDATE
  ON omop.care_site_history
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER location_dates
  BEFORE INSERT OR UPDATE
  ON omop.location
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER location_history_dates
  BEFORE INSERT OR UPDATE
  ON omop.location_history
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER fact_relationship_dates
  BEFORE INSERT OR UPDATE
  ON omop.fact_relationship
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER role_dates
  BEFORE INSERT OR UPDATE
  ON omop.role
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER job
  BEFORE INSERT OR UPDATE
  ON omop.job
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER job_definition
  BEFORE INSERT OR UPDATE
  ON omop.job_definition
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER relationship
  BEFORE INSERT OR UPDATE

  ON omop.relationship
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

create index on concept (change_datetime);
create index on vocabulary (change_datetime);
create index on domain (change_datetime);
create index on concept_class (change_datetime);
create index on concept_relationship (change_datetime);
create index on relationship (change_datetime);
create index on concept_synonym (change_datetime);
-- create index on drug_stength (change_datetime);
-- create index on attribute_definition (change_datetime);
-- create index on cdm_source (change_datetime);
-- create index on metadata (change_datetime);
create index on person (change_datetime);
-- create index on specimen (change_datetime);
create index on visit_occurrence (change_datetime);
create index on visit_detail (change_datetime);
create index on procedure_occurrence (change_datetime);
create index on condition_occurrence (change_datetime);
create index on measurement_labs (measurement_id);
create index on measurement_labs (change_datetime);
create index on measurement_physio (measurement_id);
create index on measurement_physio (change_datetime);
create index on note_section (change_datetime);
create index on note_section (note_event_id);
create index on note_content (change_datetime);
create index on note_content (note_event_id);
create index on note_nlp (note_id);
create index on note_nlp (change_datetime);
create index on observation (person_id);
create index on observation (visit_occurrence_id);
create index on observation (visit_detail);
create index on observation (change_datetime);
-- create index on survey_conduct (change_datetime);
create index on fact_relationship (fact_id_1);
create index on fact_relationship (fact_id_2);
create index on fact_relationship (domain_concept_id_1);
create index on fact_relationship (domain_concept_id_2);
create index on fact_relationship (relationship_concept_id);
create index on fact_relationship (change_datetime);
create index on drug_exposure (change_datetime);
create index on drug_exposure (drug_exposure_id);
-- create index on device_exposure (change_datetime);
create index on location (change_datetime);
create index on location_history (location_id);
create index on location_history (entity_id);
create index on location_history (change_datetime);
create index on care_site (change_datetime);
create index on care_site_history (care_site_id);
create index on care_site_history (entity_id);
create index on care_site_history (change_datetime);
create index on role (change_datetime);
create index on provider (change_datetime);
-- create index on payer_plan_period (change_datetime);
create index on cost (change_datetime);
create index on drug_era (change_datetime);
create index on cohort_definition (owner_entity_id);
create index on cohort_definition (change_datetime);
create index on cohort (cohort_definition_id);
create index on cohort (change_datetime);
create index on concept_fhir (change_datetime);
create index on concept_fhir (concept_code);
create index on job (job_id);
